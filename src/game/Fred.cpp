#include "Fred.h"
#include <cmath>

Fred::Fred()
	:
	name("Fredboy")
{
}

Fred::Fred(std::string Fname, std::vector<int> Fnumbers, Vector3 Fposition, Quaternion Forientation)
{
}

void Fred::setName(std::string fName)
{

	name = fName;

}

string Fred::getName()
{

	return name;

}

void Fred::SetPositionX(float X)
{

	position.mX = X;

}

void Fred::SetPositionY(float Y)
{

	position.mY = Y;

}

void Fred::SetPositionZ(float Z)
{

	position.mZ = Z;

}

float Fred::getpositionX()
{
	return position.mX;
}

float Fred::getpositionY()
{
	return position.mY;
}

float Fred::getpositionZ()
{
	return position.mZ;
}

void Fred::SetRotation(Quaternion rotation)
{

	orientation = rotation;

}

Quaternion Fred::getRotation()
{

	return orientation;

}

void Fred::Read(InputMemoryStream & in)
{
	int len = 0;
	char temp = 0;
	//in.Read(name);
	in.Read(position.mX);
	in.Read(position.mY);
	in.Read(position.mZ);
	//in.Read(orientation);

	in.Read(len);
	name.clear();
	for (int i = 0; i < len; i++)
	{
		in.Read(temp);
		name += temp;

	}

}

void Fred::Write(OutputMemoryStream & out)
{

	//out.Write(name);
	out.Write(position.mX);
	out.Write(position.mY);
	out.Write(position.mZ);
	//out.Write(orientation);
	int len = name.length();
	out.Write(len);

	for (char& c : name) //if this throws an error your compiler can't handle C++11!
		out.Write(c);

}
