
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"

#include "Bob.h"
#include "Fred.h"
#include <iostream>

#if _WIN32
#include <Windows.h>
#endif

using namespace std;

//#if _WIN32
//int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
//{
//	UNREFERENCED_PARAMETER( hPrevInstance );
//	UNREFERENCED_PARAMETER( lpCmdLine );
//
//
//
//}
//#else
//const char** __argv;
//int __argc;
//int main(int argc, const char** argv)
//{
//	__argc = argc;
//	__argv = argv;
//
//}
//#endif

int main(void)
{

	//serialisation part 1

	char* bufferIn = new char[256];

	InputMemoryStream *inStream = new InputMemoryStream(bufferIn, 256);
	OutputMemoryStream *outStream = new OutputMemoryStream();

	

	//Bob bob = Bob();
	//bob.Write(*outStream);



	//// Setup a buffer (out is your OutputMemoryStream)
	//int copyLen = outStream->GetLength();
	//char* copyBuff = new char[copyLen];
	//// Copy over the buffer
	//memcpy(copyBuff, outStream->GetBufferPtr(), copyLen);
	//// Create a new memory stream
	//inStream = new InputMemoryStream(copyBuff, copyLen);
	//bob.Read(*inStream);
	//
	//cout << "bob is fun " << bob.getI() << endl;

	//serialisation part 2
	Fred fred = Fred();

	fred.Write(*outStream);
	
	int copyLen = outStream->GetLength();
	char* copyBuff = new char[copyLen];
	// Copy over the buffer
	memcpy(copyBuff, outStream->GetBufferPtr(), copyLen);
	// Create a new memory stream
	inStream = new InputMemoryStream(copyBuff, copyLen);

	Fred actorFred = Fred();

	actorFred.Read(*inStream);

	cout << "Fred's name " << actorFred.getName() << endl;
	return 0;
}
