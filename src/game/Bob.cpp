#include "Bob.h"

Bob::Bob()
	:
	i(0.0f), j(0.0f), k(0.0f), x(0), y(0), z(0)
{

}


	Bob::Bob(float inI, float j, float k, int x, int y, int z)
		:
		i(inI),	j(j), k(k),	x(x), y(y), z(z)
	{

		this->i = i;
		this->j = j;
		this->k = k;
		this->x = x;
		this->y = y;
		this->z = z;
	
	}

	void Bob::SetI(float I)
	{

		this->i = I;

	}
	void Bob::SetJ(float J)
	{

		this->j = J;

	}	
	void Bob::SetK(float K)
	{

		this->k = K;

	}
	void Bob::SetPositionX(int X)
	{

		this->x = X;

	}
	void Bob::SetPositionY(int Y)
	{

		this->y = Y;

	}
	void Bob::SetPositionZ(int Z)
	{

		this->z = Z;

	}

	float Bob::getI(void)
	{

		return this->i;

	}
	float Bob::getJ(void)
	{

		return this->j;

	}
	float Bob::getK(void)
	{

		return this->k;

	}

	int Bob::getpositionX()
	{

		return this->x;

	}
	int Bob::getpositionY()
	{

		return this->y;

	}
	int	Bob::getpositionZ()
	{

		return this->z;

	}

	bool Bob::operator==(Bob& other)
	{
		if (x != other.x) return false;
		if (std::abs(i - other.i) > 0.00001f) return false;

		return true;
	}

	void Bob::Read(InputMemoryStream& in)
	{

		int len = 0;
		char temp = 0;

		in.Read(x);
		in.Read(i);

		//in.Read(len);

		//name.clear();

		//for (int i = 0; i < len; i++)
		//{
		//	in.Read(temp);
		//	name += temp;
		//}

	}

	void Bob::Write(OutputMemoryStream& out)
	{

		out.Write(x);
		out.Write(i);

		//int len = name.length();
		//out.Write(len);

		//for (char& c : name) //if this throws an error your compiler can't handle C++11!
		//	out.Write(c);

	}