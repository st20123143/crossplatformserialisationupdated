#pragma once
#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"

#include <cmath>
using namespace std;

class Fred 
{
private:

	std::string name;
	std::vector<int> numbers;
	Vector3 position;
	Quaternion orientation;
	

public:

	Fred();
	Fred(std::string Fname, std::vector<int> Fnumbers, Vector3 Fposition, Quaternion Forientation);

	void setName(std::string fName);

	string getName();

	//void setPosition(Vector3 fPosition);
	void SetPositionX(float X);
	void SetPositionY(float Y);
	void SetPositionZ(float Z);
	
	float getpositionX();
	float getpositionY();
	float getpositionZ();

	void SetRotation(Quaternion rotation);

	Quaternion getRotation();

	void Read(InputMemoryStream& in);
	void Write(OutputMemoryStream& out);

};