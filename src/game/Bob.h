#pragma once

#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"

#include <cmath>

class Bob
{
private:

	float i, j, k;
	int x, y, z;

public:

	Bob();
	Bob(float inI, float j, float k, int x, int y, int z);

	void SetI(float I);
	void SetJ(float J);
	void SetK(float K);

	void SetPositionX(int X);
	void SetPositionY(int Y);
	void SetPositionZ(int Z);

	int getpositionX();
	int getpositionY();
	int getpositionZ();
	
	float getI(void);
	float getJ(void);
	float getK(void);
	
	bool operator==(Bob& other);

	void Read(InputMemoryStream& in);
	void Write(OutputMemoryStream& out);

};

